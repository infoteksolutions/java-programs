package javaBasicsForSelenium;

public class Java2IntigerAndFloat {

public static void main(String[] args) {
// to create an intiger variable
int x=10;
int y=20;
int sum=x + y;
System.out.println(x);
System.out.println(y);
System.out.println(sum);

// best way to print both together is use "+" signe as follows
System.out.println("Sum of " + x + " and " + y + " is " + sum);

// x divide by y is 0.5, which is not intigier so use float variable
double result=(double) x/y;
System.out.println(x + "/" + y + '=' + result);

}

}