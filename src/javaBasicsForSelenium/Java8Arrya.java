package javaBasicsForSelenium;

import java.util.ArrayList;
import java.util.List;

public class Java8Arrya {

public static void main(String[] args) {
for(int i=1; i<11; i++){
System.out.println("Count is: " + i);
}

int[] numbers = {1,2,3,4,5,6,7,8,9,10};
for (int item : numbers) {
System.out.println("Count is: " + item);
}

// USING LIST
List <String> knownIT = new ArrayList<String>();
knownIT.add("ebay");
knownIT.add("Yahoo");
knownIT.add("Google");
System.out.println(knownIT);

//using for loop to print list
for(String a: knownIT){
System.out.println(a);
}

}

}