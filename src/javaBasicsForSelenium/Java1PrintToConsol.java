package javaBasicsForSelenium;

public class Java1PrintToConsol {

public static void main(String[] args) {
// console is a display board located at eclipse footer.
// to print out any message to the console
System.out.print("Halo World");
//to print any text and move to next line using '\n'
System.out.print("Halo World\n");
//to print any text and move to next line without '\n'
System.out.println("Halo World");

}

}