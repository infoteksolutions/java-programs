package javaBasicsForSelenium;

public class Java3CharacterAndString {

public static void main(String[] args) {
//character variable stores only a single letter
char thirdAlphabet='c';
char fourthAlphabet='d';
System.out.println("Third and fourht alphabets are : " + thirdAlphabet + " , " + fourthAlphabet);

// String variable is used to store one or more characters
String firstName="Solomon";
String lastName="John";

String fullName=firstName + " " + lastName ;

System.out.println("My name is :" + fullName);

}

}

