package javaBasicsForSelenium;

public class Java10Function {

public static void main(String[] args) {
// function/method is used to group some operation togethere, so that you may call any time to run
message();
String myName="Solomon";
message(myName);
int x=30, y=3;
System.out.println(x + "+" + y + "=" + add(x,y));
System.out.println(x + "-" + y + "=" + sub(x,y));
System.out.println(x + "x" + y + "=" + mul(x,y));
System.out.println(x + "/" + y + "=" + div(x,y));
}
public static void message(){
System.out.println("Halo my student");
}

// function with parameter

public static void message(String name){
System.out.println("Myname is " + name);
}
//return fucntion additon numbers
public static int add(int a, int b){
int sum= a + b;
return sum;
}

public static int sub(int a, int b){
return a - b;
}

public static int mul(int a, int b){
return a * b;
}

public static double div(int a, int b){
return (double) a/b;
}

}