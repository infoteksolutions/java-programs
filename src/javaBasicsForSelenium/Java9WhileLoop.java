package javaBasicsForSelenium;

public class Java9WhileLoop {

public static void main(String[] args) {
// Differentiate even and all numbers between 0 and 210
int i =0;
while(i<=10){
// % is used to get the remainder of a division
if(i%2==0){
System.out.println(i + " is even");
}else{
System.out.println(i + " is odd");
}
i++;
}

}

}