package javaBasicsForSelenium;

public class Calculator {

	// return fucntion additon numbers
	public int add(int a, int b) {
		int sum = a + b;
		return sum;
	}

	public int sub(int a, int b) {
		return a - b;
	}

	public int mul(int a, int b) {
		return a * b;
	}

	public double div(int a, int b) {
		return (double) a / b;
	}

	public static void main(String[] args) {
		int a = 50, b = 20;
		Calculator c = new Calculator();
		System.out.println("Sum of a and b is " + c.add(a, b));
		System.out.println("Substraction of a and b is " + c.sub(a, b));
		System.out.println("Multiplication of a and b is " + c.mul(a, b));
		System.out.println("Divison of a and b is " + c.div(a, b));
	}
}