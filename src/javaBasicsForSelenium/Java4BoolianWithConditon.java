package javaBasicsForSelenium;

public class Java4BoolianWithConditon {

public static void main(String[] args) {
// boolian vairable is to present either true(1) or false(0)
boolean result=true;
if(result){
// this will be executed if result =true
System.out.println("The result is" + result);
}else{
// this will be executed if result =false
System.out.println("The result is " + result);
}

result=false;
if(result){
// this will be executed if result =true
System.out.println("The result is " + result);
}else{
//this will be executed if result =false
System.out.println("The result is " + result);
}

}

}