package javaBasicsForSelenium;

public class Java11UsingClass {

public static void main(String[] args) {
int x=30, y=3;
Calculator cal = new Calculator();
System.out.println(x + "+" + y + "=" + cal.add(x,y));
System.out.println(x + "-" + y + "=" + cal.sub(x,y));
System.out.println(x + "x" + y + "=" + cal.mul(x,y));
System.out.println(x + "/" + y + "=" + cal.div(x,y));

}

}