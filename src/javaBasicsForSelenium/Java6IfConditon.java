package javaBasicsForSelenium;

public class Java6IfConditon {

public static void main(String[] args) {
// if... else condition is used to do some action based on condition
int testscore = 76;
char grade;

if (testscore >= 90) {
grade = 'A';
} else if (testscore >= 80) {
grade = 'B';
} else if (testscore >= 70) {
grade = 'C';
} else if (testscore >= 60) {
grade = 'D';
} else {
grade = 'F';
}
System.out.println("Grade = " + grade);
}

}