package javaBasicsForSelenium;

import java.util.Arrays;

public class Java5Array {

public static void main(String[] args) {
// array stores numbers of records together
// a string array to store student name
String [] students = new String[10];
// assign value to array
students[0]= "Solomon";
students[1]= "John";
students[3]= "Piter";
// you can add only up to 10 records
System.out.println(Arrays.toString(students));

//-----------other approach -------
String[] array1 = new String[] { "Elem1", "Elem2", "Elem3" };
System.out.println(Arrays.toString(array1));

//Print the 2nd element from array1
System.out.println("The third element in arrray1 is:" + array1[2]);

}

}